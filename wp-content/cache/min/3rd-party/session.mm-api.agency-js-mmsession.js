// https://github.com/marketlytics/trafficSourceTracker.js

(function (window, document) {
    if (/google|crawler|spider|robot|crawling|baidu|bing|msn|duckduck|teoma|slurp|yandex/i.test(navigator.userAgent)) return;

    const cookieStrKey = 'traffic_src';

    let trafficSource = {},
        mmcc,
        mmid,
        mmsid,
        getDateAfterYears = function (years) {
            return new Date(new Date().getTime() + (years * 365 * 24 * 60 * 60 * 1000));
        };

    let cookies = document.cookie.split(';');
    for (let i = 0; i < cookies.length; i++) {
        if (cookies[i].indexOf(cookieStrKey) >= 0) {
            trafficSource = cookies[i];
            trafficSource = JSON.parse(trafficSource.substring(trafficSource.indexOf('=') + 1, trafficSource.length));
        } else if (cookies[i].indexOf('mmcc') >= 0) {
            mmcc = cookies[i];
            mmcc = mmcc.substring(mmcc.indexOf('=') + 1, mmcc.length);
        } else if (cookies[i].indexOf('mmid') >= 0) {
            mmid = cookies[i];
            mmid = mmid.substring(mmid.indexOf('=') + 1, mmid.length);
        } else if (cookies[i].indexOf('mmsid') >= 0) {
            mmsid = cookies[i];
            mmsid = mmsid.substring(mmsid.indexOf('=') + 1, mmsid.length);
        }
    }

    if (mmid && mmsid) {
        fetch("https://session.mm-api.agency/view?mmsid=" + mmsid + "&url=" + window.location.href, {
                method: "GET",
            }
        ).catch(error => console.log(error));

        // if we have mmid and mmsid but not mmcc, try again
        if (!mmcc || "null" === mmcc || "undefined" === mmcc) {
            fetch("https://session.mm-api.agency/code", {
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({"ga_landing_page": document.location.href}),
                method: "POST",
            })
                .then(response => response.json())
                .then(json => {
                    document.cookie = 'mmcc=' + json.mmcc + '; expires=' + getDateAfterYears(2) + '; domain=' + window.location.hostname + '; path=/';
                })
                .catch(error => console.log(error));
        }
    } else {
        let utils = {
            /*function is use to compare two parameters and return value if valid,
             *it looks for name(any variable) in url and returns its docoded value if found in url.
             */
            getParameterByName: function (url, name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
                var results = regex.exec(url);
                return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            },

            getKeywords: function (url) {	//return empty sting if url is empty or direct, which indicate no keywords used.
                if (url === '' || url === '(direct)') return '';

                //we compare pre-define searchEngines object to find relavent keywords in url.
                var searchEngines = 'daum:q eniro:search_word naver:query pchome:q images.google:q google:q yahoo:p yahoo:q msn:q bing:q aol:query aol:q lycos:q lycos:query ask:q cnn:query virgilio:qs baidu:wd baidu:word alice:qs yandex:text najdi:q seznam:q rakuten:qt biglobe:q goo.ne:MT search.smt.docomo:MT onet:qt onet:q kvasir:q terra:query rambler:query conduit:q babylon:q search-results:q avg:q comcast:q incredimail:q startsiden:q go.mail.ru:q centrum.cz:q 360.cn:q sogou:query tut.by:query globo:q ukr:q so.com:q haosou.com:q auone:q'.split(' ');
                for (var i = 0; i < searchEngines.length; i++) {//set source of traffic to search engine
                    var val = searchEngines[i].split(':');
                    var name = val[0];
                    var queryParam = val[1];
                    if (url.indexOf(name) >= 0) {
                        trafficSource.ga_source = name; // set source to searchEngine name.
                        if (this.getParameterByName(url, queryParam) !== '') {
                            //return value of queryParamter, extracted keyowrd.
                            return this.getParameterByName(url, queryParam);
                        }
                    }
                }

                return '';
            },

            //function to set medium based on different params.
            getMedium: function (ccokieObj) {
                if (trafficSource.ga_medium !== '') return trafficSource.ga_medium;

                if (trafficSource.ga_gclid !== '') return 'cpc';

                if (trafficSource.ga_source === '') return '';

                if (trafficSource.ga_source === '(direct)') return '(none)';

                if (trafficSource.ga_keyword !== '') return 'organic';

                return 'referral';
            },


            //checking url to return approprate hostname.
            getHostname: function (url) {
                var re = new RegExp('^(https:\/\/|http:\/\/)?([^\/?:#]+)');
                var match = re.exec(url)[2];
                if (match !== null) {
                    return match;
                }
                return '';
            }
        };

        // query params keys to look for to get utm data.
        var parameters = [{
            key: 'utm_source',
            label: 'ga_source',
            required: true
        }, {
            key: 'utm_medium',
            label: 'ga_medium',
            required: false
        }, {
            key: 'utm_campaign',
            label: 'ga_campaign',
            required: false
        }, {
            key: 'utm_content',
            label: 'ga_content',
            required: false
        }, {
            key: 'utm_term',
            label: 'ga_keyword',
            required: false
        }];

        let setCookie = function () {
            trafficSource.ga_gclid = utils.getParameterByName(document.location.href, 'gclid');

            let ignoreUtmParameters = false;
            for (var i = 0; i < parameters.length; i++) {
                let value = utils.getParameterByName(document.location.href, parameters[i].key);

                if (parameters[i].required && value === '') {
                    ignoreUtmParameters = true;
                    for (var j = 0; j < parameters.length; j++) {
                        trafficSource[parameters[j]['label']] = '';
                    }
                    break;
                }
                trafficSource[parameters[i]['label']] = value;
            }

            //source is assumed to be google when gclid is present and source is NULL
            if (trafficSource.ga_gclid !== '' && trafficSource.ga_source === '') {
                trafficSource.ga_source = 'google';
            }

            //Checks if ignoreUtmParameters is true.
            else if (ignoreUtmParameters) {
                //Set the default source value '(direct)'.
                trafficSource.ga_source = document.referrer !== '' ? document.referrer : '(direct)';
            }

            trafficSource.ga_keyword = trafficSource.ga_keyword === '' ? utils.getKeywords(trafficSource.ga_source) : trafficSource.ga_keyword;
            trafficSource.ga_medium = utils.getMedium(trafficSource);
            trafficSource.ga_landing_page = document.location.href;
            trafficSource.ga_source = utils.getHostname(trafficSource.ga_source);
            trafficSource.referer = document.referrer;

            if (trafficSource.ga_source !== '') {
                document.cookie = cookieStrKey + '=; expires=' + new Date(-1);
                document.cookie = cookieStrKey + '=' + JSON.stringify(trafficSource) + '; domain=' + window.location.hostname + '; path=/';
            }

            // send any values we have
            trafficSource.mmid = mmid;
            trafficSource.mmsid = mmsid;
            trafficSource.clientCode = mmcc;
            trafficSource.userAgent = navigator.userAgent;

            fetch("https://session.mm-api.agency/init", {
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(trafficSource),
                method: "POST",
            })
                .then(response => response.json())
                .then(json => {
                    document.cookie = 'mmcc=' + json.mmcc + '; expires=' + getDateAfterYears(2) + '; domain=' + window.location.hostname + '; path=/';
                    document.cookie = 'mmid=' + json.mmid + '; expires=' + getDateAfterYears(2) + '; domain=' + window.location.hostname + '; path=/';
                    document.cookie = 'mmsid=' + json.mmsid + '; domain=' + window.location.hostname + '; path=/';
                })
                .catch(error => console.log(error));
        };

        setCookie();
    }

    // legacy
    window.trafficSrcCookie = {
        setCookie: function () {
        }
    };
})(window, document);